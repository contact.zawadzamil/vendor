<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Store Vendor
Route::post('add-vendor','App\Http\Controllers\VendorController@store')->name('vendor.store');
Route::get('vendors','App\Http\Controllers\VendorController@index')->name('vendor.index');
Route::post('vendor/{id}','App\Http\Controllers\VendorController@destroy')->name('vendor.destroy');

Route::post('add-purchase','App\Http\Controllers\PurchaseController@store')->name('purchase.store');
Route::get('purchase','App\Http\Controllers\PurchaseController@index')->name('purchase.index');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
