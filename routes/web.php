<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','App\Http\Controllers\VendorController@home')->name('welcome');
Route::get('edit-vendor/{id}','App\Http\Controllers\VendorController@edit')->name('vendor.edit');
Route::post('update-vendor/{id}','App\Http\Controllers\VendorController@update')->name('vendor.update');
Route::get('view-vendor/{id}','App\Http\Controllers\VendorController@show')->name('vendor.show');


Route::get('purchase','App\Http\Controllers\PurchaseController@home')->name('purchase');
