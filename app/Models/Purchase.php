<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'unit_price',
        'total_price',
        'quantity',
        'vendor_id',
    ];
    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }
}
