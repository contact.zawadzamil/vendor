<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $vendors = Vendor::orderBy('created_at','DESC')->get();
        return response()->json($vendors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $vendor = Vendor::create([
            'name'=>$request ->input('name'),
            'email'=>$request ->input('email'),
            'phone'=>$request ->input('phone'),
            'address'=>$request ->input('address'),
        ]);
        $vendor ->save();

        return response()->json(['success'=>'OK','vendor'=>$vendor]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $vendor = Vendor::find($id);
        return view('show-vendor')->with('vendor',$vendor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $vendor = Vendor::find($id);
       return view('edit-vendor')->with('vendor',$vendor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request,$id)
    {
       $vendor = Vendor::find($id);
       $vendor ->update([
           'name'=>$request->name,
           'email'=>$request->email,
           'phone'=>$request->phone,
           'address'=>$request->address,
       ]);
       return redirect()->route('welcome')->with('success','Vendor Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Vendor::find($id)->delete();
        return  response()->json(['success'=>'Record Deleted Successfully']);
    }

    public function home(){
        return view('welcome');
    }
}
//TODO:(Completed): Add Vendor Without Reload
//TODO:(Completed): Remove Vendor Without Reload
//TODO: (1,2) Success Message on Each Action Repair as well as Warning (Failed)
//TODO: (1,2) Edit Vendor, View Vendor
//TODO: 3,4
