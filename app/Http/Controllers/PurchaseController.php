<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use Illuminate\Http\Request;


class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $purchases = Purchase::select('purchases.*', 'vendors.name as vname')->join('vendors', 'vendors.id', 'purchases.vendor_id')->get();

        return response()->json($purchases);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $total = $request->unit_price * $request->quantity;
        $purchase = Purchase::create([
            'name'=>$request->input('name'),
            'unit_price'=>$request->input('unit_price'),
            'quantity'=>$request->input('quantity'),
            'total_price'=>$total,
            'vendor_id'=>$request->input('vendor_id'),
        ]);
        $purchase ->save();
        $vendor = $purchase->vendor;
        return response()->json(['success'=>'Purchase Added Successfully','purchase'=>$purchase,'vendor'=>$vendor]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }

    public function home(){
        return view('add-purchase');
    }
}
