<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Vendor Task</title>
</head>
<body>
<div class="container">
    <div class="heading">
        <h3 class="display-4 text-center">Vendor Project</h3>
        <div class="buttons p-4 text-center">
            <a href="{{route('welcome')}}" class="btn btn-lg btn-primary ">Add Vendor</a>

            <a href="{{route('purchase')}}" class="btn btn-lg btn-secondary ">Purchase Order</a>
        </div>

    </div>
    {{--    Form --}}
    <div class="row">
        <form action="{{route('vendor.update',$vendor->id)}}" method="POST" name="frmProduct" >
            @csrf
            {{--            Vendor Name --}}
            <div class="mb-3">
                <label for="name" class="form-label">Vendor Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$vendor->name}}"
                       required>
            </div>
            {{--                Vendor Phone --}}
            <div class="mb-3">
                <label for="phone" class="form-label">Vendor Phone No</label>
                <input type="number" name="phone" class="form-control" id="phone" value="{{$vendor->phone}}" required>
            </div>
            {{--            Email --}}

            <div class="mb-3">
                <label for="email" class="form-label">Vendor Email</label>
                <input type="email" name="email" class="form-control" id="email" value="{{$vendor->email}}"
                       required>
            </div>

            {{--            Office Address --}}
            <div class="mb-3">
                <label for="address" class="form-label">Vendor Address</label>
                <input type="text" name="address" class="form-control" id="address"
                       value="{{$vendor->address}}" required>
            </div>
            <div class="mb-3 text-center">
                <button class="btn btn-success " id="submitForm"  >
                    Update
                </button>
            </div>
        </form>

    </div>

    {{--    Data Show --}}


</div>


{{--JQUERY CDN --}}
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>

    $('#submitForm').click(function (e) {
        e.preventDefault();
        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var address = $('#address').val();


        if (phone.length < 11) {
            alert('Minimum 11 digit Phone No. Required');
        }
        else if (phone.length > 15) {
            alert('Max 15 digit Phone No. Allowed');
        }
        else{
            document.frmProduct.submit();
        }

    })
</script>

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>


</body>
</html>
