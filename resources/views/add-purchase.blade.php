<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Vendor Task</title>
</head>
<body>
<div class="container">
    <div class="heading">
        <h3 class="display-4 text-center">Vendor Project</h3>
        <div class="buttons p-4 text-center">
            <a href="{{route('welcome')}}" class="btn btn-lg btn-secondary ">Add Vendor</a>
            <a href="{{route('purchase')}}" class="btn btn-lg btn-primary ">Purchase Order</a>
        </div>
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
{{--        // Alerts --}}
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="successAlert">
            <strong>Success</strong><span class="success_message"> </span>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="errorAlert">
            <strong>Error!</strong> <span id="error_m"></span>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>



    {{--    Form --}}
    <div class="row">
        <form action="">
            @csrf
            {{--            Item Name --}}
            <div class="mb-3">
                <label for="name" class="form-label">Item Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="ex.Potato"
                       required>
            </div>
            {{--               Quantity  --}}
            <div class="mb-3">
                <label for="quantity" class="form-label">Item Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" placeholder="ex.7" min="1" required>
            </div>


            {{--            Unit Price --}}

            <div class="mb-3">
                <label for="unit_price" class="form-label">Unit Price($)</label>
                <input type="number" name="unit_price" class="form-control" id="unit_price" min="1" placeholder="ex. 100"
                       required>
            </div>

            {{--            Total Price --}}
            <div class="mb-3">
                <label for="total" class="form-label">Total Price</label>
                <input type="number" name="total" class="form-control" id="total"
                       required readonly value="0">
            </div>

{{--            Vendor --}}
            <div class="mb-3">
                <select name="vendor_id" id="vendor_id" class="form-control">
                    <option value="" selected disabled>Select Vendor</option>

                </select>
            </div>
            <div class="mb-3 text-center">
                <button class="btn btn-success " id="submit" type="button">
                    Submit
                </button>
            </div>
        </form>

    </div>

    {{--    Data Show --}}
    <div class="row">
        <table class="table table-dark table-hover" id="myTable">
            <thead>
            <tr>

                <th>Item Name</th>
                <th>Unit Price</th>
                <th>Item Quantity</th>
                <th>Total</th>
                <th>Vendor Name</th>

            </tr>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>


{{--JQUERY CDN --}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script >
    $('#successAlert').hide();
    $('#errorAlert').hide();


// Validation

    $("#quantity").on('change  paste', function () {
        var q = $('#quantity').val();
        if(q<1){
            alert('Quantity Cant be less than 1')
            $('#quantity').val('')
        }
        var price = $('#unit_price').val();
        if(q !='' && price !=''){
            $('#total').val(q*price);
        }
    });
    $("#unit_price").on('change  paste', function () {
        var q = $('#quantity').val();

        var price = $('#unit_price').val();
        if(price<1){
            alert('Quantity Cant be less than 1')
            $('#quantity').val('')
        }
        if(q !='' && price !=''){
            $('#total').val(q*price);
        }
    });

</script>

<script>
    // Add Purchase
    $('#submit').click(function () {
        var name  = $('#name').val();
        var unit_price  = $('#unit_price').val();
        var quantity  = $('#quantity').val();
        var vendor_id  = $('#vendor_id').val();


        if (unit_price < 1) {
            alert('Price cant be less than 1');
        }
        else if (quantity <1) {
            alert('Price cant be less than 1');

        }
        else {
            $.ajax({
                type: 'POST',
                url: "{{ route('purchase.store') }}",
                data: {name: name, unit_price: unit_price, quantity: quantity, vendor_id: vendor_id},
                success: function (data) {


                    $('.success_message').text('Purchase Added Successfully');
                    $('#successAlert').show();
                    $('#errorAlert').hide();
                    $('#name').val('');
                    $('#unit_price').val('');
                    $('#quantity').val('');
                    $('#total').val('');
                    $('#vendor_id').val('');
                    $('#myTable tbody')
                        .prepend('<tr />')
                        .children('tr:first')
                        .append('<td >'+data.purchase.name+'</td> <td >'+data.purchase.unit_price+'</td ><td >'+data.purchase.quantity+'</td > <td>'+data.purchase.total_price+'</td > <td>'+data.vendor.name+'</td >')


                },
                error: function (xhr, textStatus, errorThrown) {

                    $('#successAlert').hide();
                    $('#errorAlert').show();
                    $('#error_m').text('Mission Failed');
                }
            });
        }


    })
</script>
<script >
    // Show Purchases
    function showTable() {
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{route('purchase.index')}}',
            success: function (data) {
                data.forEach(element=>{
                    $("#myTable > tbody").append("<tr>" +
                        "<td>"+element.name +"</td>" +
                        "<td>"+element.unit_price+"</td>" +
                        "<td>"+element.quantity+"</td>" +
                        "<td>"+element.total_price+"</td>" +
                        "<td>"+element.vname+"</td>" +
                        "</tr>");
                });
            },
            error: function() {
                console.log(data);
            }
        });
    }
    showTable();
</script>
<script >
    // Show Vendors
    function showTable() {
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{route('vendor.index')}}',
            success: function (data) {

                data.forEach(element=>{
                    $('#vendor_id').append($('<option>', {
                        value: element.id,
                        text: element.name
                    }));
                });


            },
            error: function() {
                console.log(data);
            }
        });
    }
    showTable();
</script>




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>


</body>
</html>
