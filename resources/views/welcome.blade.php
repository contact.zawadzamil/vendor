<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Vendor Task</title>
</head>
<body>
<div class="container">
    <div class="heading">
        <h3 class="display-4 text-center">Vendor Project</h3>
        <div class="buttons p-4 text-center">
            <a href="{{route('welcome')}}" class="btn btn-lg btn-primary ">Add Vendor</a>
            <a href="{{route('purchase')}}" class="btn btn-lg btn-secondary ">Purchase Order</a>
        </div>
        @if(session('success'))
            <div class="alert alert-success">
               {{session('success')}}
            </div>
        @endif
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="successAlert">
            <strong>Success</strong><span class="success_message"> </span>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="errorAlert">
            <strong>Error!</strong> <span id="error_m"></span>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    {{--    Form --}}
    <div class="row">
        <form action="">
            @csrf
            {{--            Vendor Name --}}
            <div class="mb-3">
                <label for="name" class="form-label">Vendor Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="ex. Mr. Zawad Zamil"
                       required>
            </div>
            {{--                Vendor Phone --}}
            <div class="mb-3">
                <label for="phone" class="form-label">Vendor Phone No</label>
                <input type="number" name="phone" class="form-control" id="phone" placeholder="ex.01781390908" required>
            </div>
            {{--            Email --}}

            <div class="mb-3">
                <label for="email" class="form-label">Vendor Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="ex.example@gmail.com"
                       required>
            </div>

            {{--            Office Address --}}
            <div class="mb-3">
                <label for="address" class="form-label">Vendor Address</label>
                <input type="text" name="address" class="form-control" id="address"
                       placeholder="ex.Nikunja 2, Dhaka 1207" required>
            </div>
            <div class="mb-3 text-center">
                <button class="btn btn-success " id="submit" type="button">
                    Submit
                </button>
            </div>
        </form>

    </div>

    {{--    Data Show --}}
    <div class="row">
        <table class="table table-dark table-hover" id="myTable">
            <thead>
            <tr>

                <th>Vendor Name</th>
                <th>Vendor Email</th>
                <th>Vendor Phone</th>
                <th>Vendor Address</th>
                <th>Action</th>
            </tr>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>


{{--JQUERY CDN --}}
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script >
    $('#successAlert').hide();
    $('#errorAlert').hide();
</script>

<script>
    // Add Vendors
    $('#submit').click(function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var address = $('#address').val();


        if (phone.length < 11) {
            alert('Minimum 11 digit Phone No. Required');
        }
       else if (phone.length > 15) {
            alert('Max 15 digit Phone No. Allowed');
        }
        else {
            $.ajax({
                type: 'POST',
                url: "{{ route('vendor.store') }}",
                data: {name: name, phone: phone, email: email, address: address},
                success: function (data) {

                    $('.success_message').text('Vendor Added Successfully');
                    $('#successAlert').show();
                    $('#errorAlert').hide();
                    $('#name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#address').val('');
                    $('#myTable tbody')
                        .prepend('<tr />')
                        .children('tr:first')
                        .append('<td >'+data.vendor.name+'</td> <td >'+data.vendor.email+'</td ><td >'+data.vendor.phone+'</td > <td>'+data.vendor.address+'</td ><td><a class="btn btn-sm  btn-info edit" id='+data.vendor.id+'>Edit</a>' +
                            '<a class="btn btn-sm  btn-danger delete ms-2"  data-id='+data.vendor.id+'>Delete</a>' +
                            '<a class="btn btn-sm  btn-warning view ms-2"  id='+data.vendor.id+'>View</a></td>')


                },
                error: function (xhr, textStatus, errorThrown) {

                    $('#successAlert').hide();
                    $('#errorAlert').show();
                    $('#error_m').text('Email Already Exists!');
                }
            });
        }


    })
</script>
<script >
    // Show Vendors
   function showTable() {
       $.ajax({
           type: 'GET', //THIS NEEDS TO BE GET
           url: '{{route('vendor.index')}}',
           success: function (data) {

               data.forEach(element=>{


                   $("#myTable > tbody").append("<tr>" +
                       "<td>"+element.name +"</td>" +
                       "<td>"+element.email+"</td>" +
                       "<td>"+element.phone+"</td>" +
                       "<td>"+element.address+"</td>" +
                       "<td><a  class='btn btn-sm  btn-info edit' id='"+element.id+"'>Edit</a>" +
                       "<a class='btn btn-sm btn-danger ms-2 delete' data-id='"+element.id+"'>Delete</a>" +
                       "<a class='btn btn-sm btn-warning ms-2 view' id='"+element.id+"'>View</a></td></tr>");
               });


           },
           error: function() {
               console.log(data);
           }
       });
   }
   showTable();
</script>

<script>
    // Edit Vendor
    $(document).on('click','.edit',function(){
        var url = "{{ url('edit-vendor') }}/"+this.id;
        location.href = url;
    });
    // Show Vendor
    $(document).on('click','.view',function(){
        var url = "{{ url('view-vendor') }}/"+this.id;
        location.href = url;
    });
    // Delete Vendor
    $('#myTable').on('click','td .delete',function(e){
        e.preventDefault();
        $(this).parents('tr').remove();
        var id = $(this).data("id");

        $.ajax(
            {

                url: "http://127.0.0.1:8000/api/vendor/"+id,
                type: 'POST',
                data:{
                    _token:'{{ csrf_token() }}'
                },
                success: function (data){
                    $(this).parents('tr').remove();


                }
            });
    });


</script>


<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>


</body>
</html>
